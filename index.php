<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<?php
	header('Content-Type: text/html; charset=utf-8');
	require_once 'includes/config.inc.php';
	require_once 'includes/utils.inc.php';
	echo "<title>$page_title</title>";
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
?>

<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/styles.css" rel="stylesheet" type="text/css" media="all">
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="script/bootstrap.min.js"></script>

</head>
<body role="document">

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="/"><img src="img/boki-sm.png" alt="<?php echo $page_title ?>"/></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="index.php?showNews=true">News</a></li>
					<li><a href="https://steamcommunity.com/groups/bokics/discussions">Forum</a></li>
					<li><a href="https://steamcommunity.com/groups/bokics">Steam Group</a></li>
					<li><a href="index.php?showDonate=true">Donate</a></li>
					<li><a href="index.php?showTop=true">Top 50</a></li>
					<form action="search.php" method="GET" class="navbar-form navbar-right">
						<div class="form-group">
						<input type="text" name="searchquery" placeholder="Player Name" class="form-control">
						</div>
						<button type="submit" class="btn btn-default">Search</button>
					</form>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">

		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- CSGO -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-5518761948172474"
		     data-ad-slot="9112347942"
		     data-ad-format="auto"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>

		<?php
			if($_GET['showDonate']==true){
				include 'donate.php';
			}else if ($_GET['showNews']==true){
				include 'news.php';
			}else if (isset($_GET['id']) && $_GET['id']==true && isset($_GET['retakes']) && $_GET['retakes']==true){
				$id = $_GET['id'];
				include 'retakesstats/showplayer.php';
			}else if (isset($_GET['showTop']) && $_GET['showTop']==true && isset($_GET['retakes']) && $_GET['retakes']==true){
				echo "<div class=\"btn-group top50btng pull-right\" role=\"group\"><a href=\"index.php?showTop=true\" class=\"btn btn-default\">1v1 Arena</a><button class=\"btn btn-primary\">Retakes</button></div>";
				include 'retakesstats/index.php';
			}else if (isset($_GET['showTop']) && $_GET['showTop']==true){
				echo "<div class=\"btn-group top50btng pull-right\" role=\"group\"><button class=\"btn btn-primary\">1v1 Arena</button><a href=\"index.php?showTop=true&retakes=true\" class=\"btn btn-default\">Retakes</a></div>";
				include 'includes/generatetopplayers.php';
			}else if (isset($_GET['id']) && !empty($_GET['id'])){
				$totalplayers = mysqli_num_rows(mysqli_query($connect, "SELECT * FROM $mysql_table WHERE (UNIX_TIMESTAMP()-lastTime)/86400 < $days_until_inactivity"));
				$run_query = "SELECT s1.*, (SELECT COUNT(*) FROM $mysql_table AS s2 WHERE s2.rating > s1.rating AND (UNIX_TIMESTAMP()-s2.lastTime)/86400 < $days_until_inactivity)+1 AS rank FROM $mysql_table AS s1 WHERE  (UNIX_TIMESTAMP()-s1.lastTime)/86400 < $days_until_inactivity AND accountID=".(int)$_GET['id'];
				$query = mysqli_query($connect, $run_query);
				if (mysqli_num_rows($query) > 0){
					if ($query){
						while ($row = mysqli_fetch_assoc($query)){
							$accountID = $row['accountID'];
							$auth = $row['auth'];
							$rank = $row['rank'];
							$name = htmlentities($row['name']);
							$wins = $row['wins'];
							$losses = $row['losses'];
							$rating = $row['rating'];
							$rifleRating = $row['rifleRating'];
							$pistolRating = $row['pistolRating'];
							$awpRating = $row['awpRating'];
							$scoutRating = $row['scoutRating'];
							$deagleRating = $row['deagleRating'];
							$lastTime = $row['lastTime'];
							$communityId = GetCommunityID($auth);
							$playerInfo = getPlayerInfo($communityId);

							if ($losses == 0) {
								$WL = $wins;
							} else{
								$WL = round($wins/$losses, 2);
							}
							echo "<div class=\"jumbotron\"><div class=\"row\"><h1><a class=\"black\" href=\"http://steamcommunity.com/profiles/".$communityId."\">$name</a></h1>";
							echo "<div class=\"pull-left avatar\"><img src=\"".$playerInfo['avatarfull']."\" height=\"184\" width=\"184\"/></div>";
							echo "<div class=\"pull-left\"><h3>Overall Rating: $rating ( $rank of $totalplayers )</h3>";
							echo "<h3>Wins: $wins &nbsp;&nbsp;&nbsp;&nbsp; Losses: $losses &nbsp;&nbsp;&nbsp;&nbsp; W/L Ratio: $WL</h3>";
							echo "<h3>Rifle Rating: $rifleRating</h3>";
							echo "<h3>Pistol Rating: $pistolRating</h3>";
							echo "<h3>AWP Rating: $awpRating</h3>";
							echo "<h3>Scout Rating: $scoutRating</h3>";
							echo "<h3>Deagle Rating: $deagleRating</h3>";
							echo "</div></div></div>";
							
						}
					}
				}else{
					die("<h2>I think you got lost</h2>");
				}
			}else{
				include 'home.php';
			}
		?>
	</div>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-62769231-1', 'auto');
		ga('send', 'pageview');
	</script>
</body>
</html>