<?php
require_once 'config.inc.php';

function GetCommunityID($steamid) {
	$steamidexplode = explode(':', str_replace('STEAM_', '' ,$steamid));
	$commid = substr(bcadd(bcadd('76561197960265728', $steamidexplode['1']), bcmul($steamidexplode['2'], '2')),0, 17);
	return $commid;
}

function getPlayerInfo($commid) {
	global $SteamAPI_Key;
	$url = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key='.$SteamAPI_Key.'&steamids='.$commid;
	$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
	$result = file_get_contents($url, false, $context);
	$json = json_decode($result, true);
	return $json['response']['players'][0];
}

function getAvatar($commid) {
	global $SteamAPI_Key;
	$url = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key='.$SteamAPI_Key.'&steamids='.$commid;
	$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
	$result = file_get_contents($url, false, $context);
	$json = json_decode($result, true);

	foreach ($json['response']['players'] as $item) {
		return $item['avatarfull'];
	}
}

function getPlayerState($commid) {
	global $SteamAPI_Key;
	$url = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key='.$SteamAPI_Key.'&steamids='.$commid;
	$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
	$result = file_get_contents($url, false, $context);
	$json = json_decode($result, true);

	foreach($json['response']['players'] as $item) {
		if($item['personastate'] == 0){
			return "offline";
		}else{
			return "online";
		}
	}
}

function isPlayerActive($lastTime) {
	$elapsedtime_days = number_format((float)(time() - $lastTime) / 86400, 2, '.', '');
	global $days_until_inactivity;
	if ($elapsedtime_days > $days_until_inactivity && $lastTime != 0) {
		return 'true';
	}
}
?>
