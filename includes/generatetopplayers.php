<?php
require 'includes/config.inc.php';

$roundTypeGet = $_GET['roundType'];
$roundType = $roundTypeGet . "rating";
$generated = "";
if($roundTypeGet == "" or $roundTypeGet == "Rifle" or $roundTypeGet == "AWP" or $roundTypeGet == "Pistol" or $roundTypeGet == "Scout" or $roundTypeGet == "Deagle"){
	$generated .= "<div class=\"top50h\">".($roundTypeGet = "" ? "<h2><strong>Top 50 1v1 Players</strong></h2>" : "<h2><strong>Top 50 1v1 " . $roundTypeGet . " Players</strong></h2>")."</div>";
	$generated .= "<table class=\"table table-condensed table-bordered table-striped\" width=\"450px\">
		<tr>
			<th>#</th>
			<th width=\"200\">Name</th>
			<th>Wins</th>
			<th>Losses</th>
			<th>W/L Ratio</th>
			<th".(($roundType == 'Riflerating') ? " class='activeTd'" : "")."><a href=\"index.php?showTop=true&roundType=Rifle\">Rifle Rating</a></th>
			<th".(($roundType == 'Pistolrating') ? " class='activeTd'" : "")."><a href=\"index.php?showTop=true&roundType=Pistol\">Pistol Rating</a></th>
			<th".(($roundType == 'AWPrating') ? " class='activeTd'" : "")."><a href=\"index.php?showTop=true&roundType=AWP\">AWP Rating</a></th>
			<th".(($roundType == 'Scoutrating') ? " class='activeTd'" : "")."><a href=\"index.php?showTop=true&roundType=Scout\">Scout Rating</a></th>
			<th".(($roundType == 'Deaglerating') ? " class='activeTd'" : "")."><a href=\"index.php?showTop=true&roundType=Deagle\">Deagle Rating</a></th>
			<th".(($roundType == 'rating') ? " class='activeTd'" : "")."><a href=\"index.php?showTop=true\">Overall Rating</a></th>
		</tr>";


	$run_query = "SELECT s1.*, (SELECT COUNT(*) FROM $mysql_table AS s2 WHERE s2.$roundType > s1.$roundType AND s2.wins+s2.losses > 200 AND (UNIX_TIMESTAMP()-s2.lastTime)/86400 < $days_until_inactivity)+1 AS rank FROM $mysql_table AS s1 WHERE s1.wins+s1.losses > 200 AND (UNIX_TIMESTAMP()-s1.lastTime)/86400 < $days_until_inactivity ORDER BY $roundType DESC LIMIT 0, 50";
	$query = mysqli_query($connect, $run_query);

	if (@$query){
		for ($i = 0; $i < 50; $i++){
			$row = mysqli_fetch_assoc($query);
			$accountID = $row['accountID'];
			$rank = $row['rank'];
			$name = htmlentities($row['name']);
			$wins = $row['wins'];
			$losses = $row['losses'];
			$rating = $row['rating'];
			$rifleRating = $row['rifleRating'];
			$pistolRating = $row['pistolRating'];
			$awpRating = $row['awpRating'];
			$scoutRating = $row['scoutRating'];
			$deagleRating = $row['deagleRating'];
			$lastTime = $row['lastTime'];

			if ($losses == 0) {
				$WL = $wins;
			} else{
				$WL = round($wins/$losses, 2);
			}

				$generated .= "<tr><td>$rank</td>";

				$generated .= "<td><a href=\"index.php?id=".$accountID."\"><span class=\"glyphicon glyphicon-chevron-right\"></span> $name</a></td>";

				$generated .= "<td>$wins</td>
								<td>$losses</td>
								<td>$WL</td>
								<td".(($roundType == 'Riflerating') ? " class='activeTd'" : "").">$rifleRating</td>
								<td".(($roundType == 'Pistolrating') ? " class='activeTd'" : "").">$pistolRating</td>
								<td".(($roundType == 'AWPrating') ? " class='activeTd'" : "").">$awpRating</td>
								<td".(($roundType == 'Scoutrating') ? " class='activeTd'" : "").">$scoutRating</td>
								<td".(($roundType == 'Deaglerating') ? " class='activeTd'" : "").">$deagleRating</td>
								<td".(($roundType == 'rating') ? " class='activeTd'" : "").">$rating</td></tr>";
			
		}
		$generated .= "</table><br>Ranked by ELO Rating.";
	}
}
echo $generated;
?>