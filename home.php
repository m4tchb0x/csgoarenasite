<h2>Welcome to Boki's Server</h2>

<p>All my servers are hosted on a private dedicated server, no sharing with anyone else so you get the fastest experience possible. If you have any suggestions or problems, feel free to contact me at me@bojancode.com</p>

<h4>List of my servers:</h4>
<!--ul>
	<li><strong> 1v1 Splewis : </strong> 198.27.64.110:27016</li>
	<li><strong> FFA Deathmatch : </strong> 198.27.64.110:27015</li>
	<li><strong> Retakes #1: </strong> 198.27.64.110:27018</li>
	<li><strong> Retakes #2: </strong> 198.27.64.110:27019</li>
	<li><strong> Private : </strong> 198.27.64.110:27017 *Donators Get Admin Access*</li>

</ul-->
<?php
	include 'serverstatus.php';
?>
<div class="pull-left">
	<div class="halfdiv pull-left">
		<h3>Player Rules</h3>
		<p>
			<ul>
				<li>Most important, Good Luck & Have Fun</li>
				<li>Treat others like you want to be treated</li>
				<li>No cheating, hacking, exploiting or ghosting</li>
				<li>Try not to be annoying on the mic, if people are complaining. Stop.</li>
				<li>No recruiting or advertising.</li>
				<li>If you are caught trying to crash or lag the server you will be permanently banned.</li>
			</ul>
		</p>
	</div>
	<div class="halfdiv pull-left">
		<h3>Admin/Mod Rules</h3>
		<p>
			<ul>
				<li>Be like the dude from The Big Lebowski</li>
				<li>Warn people first, if they dont listen. Take necessary action (beacon, slay, kick, etc.)</li>
				<li>If the server is near full, kick people who are AFK for multiple rounds to make room for others</li>
				<li>Admins must abide by the same rules as players.</li>
				<li>If someone is hacking, take a demo first then you can ban if you have the power</li>
				<li>Do not cause trouble or abuse your power</li>
				<li>Abuse your power, and you will be stripped of it</li>
			</ul>
		</p>
	</div>
</div>