<br/>
<?php
	$url = "https://steamcommunity.com/groups/bokics/rss/" ;
	$rss = simplexml_load_file($url); // XML parser
    foreach($rss->channel->item as $entry) {
        echo "<div class='well'>";
        echo "<h2><a href='$entry->link' title='$entry->title'>" . $entry->title . "</a></h2>";
        echo "<h4>" . (new DateTime($entry->pubDate))->format('D, d M Y') . " by " . $entry->author . "</h4>";
        echo "<p>" . $entry->description . "</p>";
        echo "</div>";
    }
?>