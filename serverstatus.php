<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Boki's Server Statues</title>
    <style>
        .leftcolumn { width: 79%; float: left;font-size: 22px;}
        .playercolumn { width: 20%; float: right;font-size: 36px; text-align:right;font-weight: bold;}
        .nonalt {display:flex; padding:4px; }
        .alt { display:flex; background-color: #DEDEDE ; padding:4px;}
        .text {white-space: nowrap;overflow: hidden;text-overflow: ellipsis;}
        .clear { clear: both;}
    </style>
</head>
<body>
<?php
$page = $_SERVER['PHP_SELF'];
if (isset($_GET['refresh']) && !empty($_GET['refresh']) && $_GET['refresh'] == 'true'){
    $sec = "10";
    header("Refresh: $sec; url=$page"."?refresh=true");
}


/* SOURCE ENGINE QUERY FUNCTION, requires the server ip:port */
function source_query($ip){
    $cut = explode(":", $ip);
    $HL2_address = $cut[0];
    $HL2_port = $cut[1];

    $HL2_command = "\377\377\377\377TSource Engine Query\0";
    
    $HL2_socket = fsockopen("udp://".$HL2_address, $HL2_port, $errno, $errstr,3);
    fwrite($HL2_socket, $HL2_command); 
    $JunkHead = fread($HL2_socket,4);
    $CheckStatus = socket_get_status($HL2_socket);

    if($CheckStatus["unread_bytes"] == 0)return 0;

    $do = 1;
    $HL2_stats = '';
    while($do){
        $str = fread($HL2_socket,1);
        $HL2_stats.= $str;
        $status = socket_get_status($HL2_socket);
        if($status["unread_bytes"] == 0){
               $do = 0;
        }
    }
    fclose($HL2_socket);

    $x = 0;
    $result = '';
    while ($x <= strlen($HL2_stats)){
        $x++;
        $result.= substr($HL2_stats, $x, 1);    
    }
    
    // ord ( string $string );
    $result = str_split($result);
    $info['network'] = ord($result[0]);$char = 1;
    while(ord($result[$char]) != "%00"){$info['name'] .= $result[$char];$char++;}$char++;
    while(ord($result[$char]) != "%00"){$info['map'] .= $result[$char];$char++;}$char++;
    while(ord($result[$char]) != "%00"){$info['dir'] .= $result[$char];$char++;}$char++;
    while(ord($result[$char]) != "%00"){$info['description'] .= $result[$char];$char++;}$char++;
    $info['appid'] = ord($result[$char].$result[($char+1)]);$char += 2;        
    $info['players'] = ord($result[$char]);$char++;    
    $info['max'] = ord($result[$char]);$char++;    
    $info['bots'] = ord($result[$char]);$char++;    
    $info['dedicated'] = ord($result[$char]);$char++;    
    $info['os'] = chr(ord($result[$char]));$char++;    
    $info['password'] = ord($result[$char]);$char++;    
    $info['secure'] = ord($result[$char]);$char++;    
    while(ord($result[$char]) != "%00"){$info['version'] .= $result[$char];$char++;}
    
    return $info;
}
$ips = array('198.27.64.110:27016','198.27.64.110:27018','198.27.64.110:27019','198.27.64.110:27015','198.27.64.110:27017');

//$ip = '198.27.64.110:27016';
//$query = source_query(); // $ip MUST contain IP:PORT
$alt = 0;
foreach ($ips as &$ip) {
    $q = source_query($ip);
    //echo "network: ".$q['network']."<br/>";
    echo ($alt % 2 ? "<div class=\"nonalt\">" : "<div class=\"alt\">");
    echo "<a href=\"steam://connect/".$ip."\" style=\"width:100%;color:black\" class=\"popup_item\">";
    echo "<div class=\"leftcolumn\"><div class=\"text\">".$q['name']."</div>";
    echo "<div class=\"text\">".$q['map']."</div></div>";
    //echo "dir: ".$q['dir']."<br/>";
    //echo "desc: ".$q['description']."<br/>";
    //echo "id: ".$q['appid']."<br/>";
    echo "<div class=\"playercolumn\">".(intval($q['players'])-intval($q['bots']));
    echo " / ".$q['max']."</div><div class\"clear\"></div>";
    echo "</a>";
    echo "</div>";
    $alt++;
    //echo "bots: ".$q['bots']."<br/>";
    //echo "dedicated: ".$q['dedicated']."<br/>";
    //echo "os: ".$q['os']."<br/>";
    //echo "password: ".$q['password']."<br/>";
    //echo "secure: ".$q['secure']."<br/>";
    //echo "version: ".$q['version']."<br/>"; 
}
?>
</body>

</html>